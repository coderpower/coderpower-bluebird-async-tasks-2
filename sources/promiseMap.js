var Promise = require('bluebird');
var getRequestToUrl = require('./getRequestToURL');

module.exports = function promiseMap() {

    var url1 = 'http://www.coderpower.com/test1';
    var url2 = 'http://www.coderpower.com/test2';

    return Promise.map([url1, url2], getRequestToUrl)
    .then(function(arrayOfWebpagesContent) {
        console.log(arrayOfWebpagesContent);
    });

};