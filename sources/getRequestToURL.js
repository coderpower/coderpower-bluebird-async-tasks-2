module.exports = function getRequestToUrl(url) {

    var url1 = 'http://www.coderpower.com/test1';
    var url2 = 'http://www.coderpower.com/test2';

    return new Promise(function(resolve, reject) {
        if (url === url1) {
            resolve('test1 ok');
        } else if (url === url2) {
            resolve('test2 ok');
        } else {
            reject('404');
        }
    });

};