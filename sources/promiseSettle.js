var Promise = require('bluebird');
var getRequestToUrl = require('./getRequestToURL');

module.exports = function promiseSettle() {

    var url1 = 'http://www.coderpower.com/test1';
    var url2 = 'http://www.coderpower.com/test2';

    function filterer(promiseInspection) {
        return promiseInspection.isFulfilled();
    }

    function displayContentOfInspections(promiseInspections) {
        promiseInspections.forEach(function(promiseInspection) {
            console.log(promiseInspection.value());
        })
    }

    var promises = [url1, url2, ''].map(getRequestToUrl);

    return Promise.settle(promises)
    .filter(filterer)
    .then(displayContentOfInspections);
};