### Async Tasks 2

Now let’s say we have a function, “getRequestToUrl”, that is fetching the content of a web page from a given URL, and returns the content through a promise. If the given url is null or empty, the function will throw an error.

Now we will create an array of 2 url, then use the Promise.map method, with the “getRequestUrl” function. The Promise.map method will try to resolve the promises automatically.

As you can see, the function getRequestToUrl returns the content of each url, replacing the initial array items by the result of the promise.

```javascript
    var url1 = 'http://www.coderpower.com/test1';
    var url2 = 'http://www.coderpower.com/test2';

    return Promise.map([url1, url2], getRequestToUrl)
    .then(function(arrayOfWebpagesContent) {
        console.log(arrayOfWebpagesContent);
    });
```


Now we add an empty url, which will cause a rejection from the “getRequestToUrl” function.
As expected, an unhandled rejection appears.

This time, we will use the standard Array.prototype.map function to create an array of promises without resolving them.

With Promise.Settle(), all promises will be triggered, and a list of object representing the state of those promises, called promiseInspection will be returned. Even if a rejection is triggered, the method Promise.settle() won’t let the rejection bubble up. Instead, it will returns an array of PromiseInspection, giving the state of the promise.

```javascript
    var promises = [url1, url2, ''].map(getRequestToUrl);

    Promise.settle(promises)
```

We can now filter the promises to return only the resolved, by using the method isFullfiled().

Let’s summarize, we have an array of promises, which will be triggered by the Promise.settle() method, then filtered to get only the resolved one, and finally display the value of resolved promises.

```javascript
    function filterer(promiseInspection) {
        return promiseInspection.isFulfilled();
    }

    function displayContentOfInspections(promiseInspections) {
        promiseInspections.forEach(function(promiseInspection) {
            console.log(promiseInspection.value());
        })
    }

    var promises = [url1, url2, ''].map(getRequestToUrl);

    Promise.settle(promises)
    .filter(filterer)
    .then(displayContentOfInspections);
```

As we can see, the error is not displayed.
This is the end of our tour of Async tasks on Collection, feel free to give a try to our associated exercice.
See you next time, and good luck.

For more information, please refer to the documentation: http://bluebirdjs.com/docs/api-reference.html
